import random
import re
from datetime import date
import time

month = date.today().strftime("%m")
year = date.today().strftime("%y")

f1 = open("position.sql","w", encoding="utf-8")
f2 = open("inserts.sql","w", encoding="utf-8")
f3 = open("skills.sql","w", encoding="utf-8")
f4 = open("indeed100.txt","r")
f5 = open("GICS.sql","w", encoding="utf-8")
f6 = open("company.sql","w", encoding="utf-8")
f2.write("DROP TABLE jobs;\nDROP TABLE company;\nDROP TABLE gics;\nDROP TABLE requires;\nDROP TABLE skills;\nDROP TABLE position;\n")
f1.write("set define off;\nCREATE TABLE position(\n   pid number(3) NOT NULL PRIMARY KEY,\n  pname VARCHAR(100),\n sdates varchar(20),\n    edates varchar(20),\n   paylow number(10),\n    payhigh number(10),\n   paytype varchar(10),\n  Jobdesc VARCHAR(4000));\n")
f2.write("\nset define off;\nCREATE TABLE position(\n   pid number(3) NOT NULL PRIMARY KEY,\n  pname VARCHAR(100),\n sdates varchar(20),\n    edates varchar(20),\n   paylow number(10),\n    payhigh number(10),\n   paytype varchar(10),\n  Jobdesc VARCHAR(4000));\n")

f2.write("\nCREATE TABLE requires(\nrsid number(3) , skillcode number (3),constraint rskillfk FOREIGN KEY (rsid) references position(pid));\n")
f2.write("\nCREATE TABLE company(\ncid number(3) NOT NULL PRIMARY KEY, \ncname varchar(100), \nlocation varchar(100),\nigid number(10),\ngid number(10),\nWebsite varchar (1000));\n")
f2.write("\nCREATE TABLE skills(\nsid number(10) NOT NULL PRIMARY KEY,\nsname varchar (40));\n")
f2.write("\nCREATE TABLE gics(\ngicsid number(10) NOT NULL PRIMARY KEY,\ngname varchar (20),\nparentid number(10));\n")

f6.write("\nCREATE TABLE company(\ncid number(3) NOT NULL PRIMARY KEY, \ncname varchar(100), \nlocation varchar(100),\nigid number(10),\ngid number(10),\nWebsite varchar (1000));\n")
f2.write("\nCREATE TABLE jobs(\njid number(3) NOT NULL PRIMARY KEY, \nposid number(3), \nskillid number(3),\ncompanyid number(3), \nemployeetype varchar (20),\nconstraint positionfk FOREIGN KEY (posid)References position(pid),\nconstraint companyfk FOREIGN KEY (companyid)References company(cid),\nconstraint skillfk FOREIGN key(skillid) References skills(sid));\n")
f3.write("DROP TABLE skills;\nCREATE TABLE skills(\nsid number(2) NOT NULL PRIMARY KEY,\nskill_name varchar(50));")
f2.write("\nCREATE TABLE course(\ncourseid number(5) NOT NULL PRIMARY KEY,\ncoursename varchar (20),\n skillcode number (10));\n\n\nCREATE TABLE section(\nsectionid number(5) NOT NULL PRIMARY KEY,\ncorid number (5),\nconstraint sectionfk FOREIGN KEY (corid) References course(courseid),\nskillcode number (10));\nCREATE table PREREQUISITE (\ncourseid number(5),\npcorse number (5));\n")


spaylrange = {"analyst":50000,"agent":10000, "help":15000,"manager":30000,"auditor":20000,"developer":50000, "specialist":40000, "director":80000}
spayhrange = {"analyst":70000,"agent":20000, "help":30000,"manager":50000,"auditor":40000,"developer":70000, "specialist":60000, "director":100000}
hpaylrange = {"analyst":20,"agent":10, "help":15,"manager":20,"auditor":20,"developer":20, "specialist":20, "director":60 , "tech":15}
hpayhrange = {"analyst":30,"agent":20, "help":25,"manager":40,"auditor":40,"developer":40, "specialist":40, "director":80 , "tech":25}
jskills= {"analyst":"support","agent":"sales", "help":"customer service","manager":"leadership","auditor":"organization","developer":"probleming solving", "specialist":"trained", "director":"corprate management"}
skills = {"programming":["object oreinted programming","IDE","version control software"],"security":["cybersecurity", "cryptology"],"network":["knowledge of networking software","knowledge of networking hardware"],
        "information technology":["knowledge of software","knowledge of pc hardware"], "qa":["unit testing","coding"],"support":["pc hardware","customer service"]}

skillz =[]
scount=0
for zkill in jskills:
    f2.write("INSERT INTO skills VALUES ("+str(scount+1)+",\'"+jskills[zkill]+"\');\n")
    f3.write("INSERT INTO skills VALUES ("+str(scount+1)+",\'"+jskills[zkill]+"\');\n")
    skillz.append(jskills[zkill])
    scount= scount+1

for skill in skills:
    for subskill in skills[skill]:
        f2.write("\nINSERT INTO skills VALUES ("+str(scount+1)+",\'"+subskill+"\');")
        f3.write("\nINSERT INTO skills VALUES ("+str(scount+1)+",\'"+subskill+"\');")
        skillz.append(subskill)
        scount= scount+1
count =1001
for skill in skillz:
    f2.write("INSERT INTO course VALUES ("+str(scount)+",\'"+skill+"\',"+str(skillz.index(skill)+1)+");\n")
    for i in range(3):
        f2.write("INSERT INTO section VALUES ("+str(i+1)+","+str(scount)+","+str(skillz.index(skill)+1)+");\n")

    if scount != 1001:
        f2.write("INSERT INTO PREREQUISITE VALUES ("+str(scount)+","+str(scount-1)+");\n")
    scount= scount+1
# subindustry
medical = ["hospital","health","medical","Dentistry"]
rehab = ["odyssey house","volunteers of america"]
hotels = ["hyatt","the troubadour","Hotels","Guesthouse"]
conventationcenter = ["center"]
marketing1 = ["design","marketing"]
informationtech= ["network","technology","security","caci","tech","systems","CGI","Epsilon","comtecinfo","PSI Pax","Booz Allen Hamilton","CSRA"]
software=["microsoft",'software','consult',"Sparkhound",'solutions',"Kofax","zlien"]
bank=["federal", "bank"]
tax =["CapinCrouse","Deloitte"]
realty = ["Realty","The Lynd Company"]
insurance = ["gilsbar","insurance"]
internet = ["at&t"]
energy = ["entergy"]
si =["equiptment","hewlett packard","Dell"]
equiptment = ["laitram","General Electric","hornbeck offshore","MECO","PosiGen"]
nonprofit = ["NRLC"]
louisiana =["government"]
highered = ["bay state","university"]
k_through_12 = ["Education"]
electronics = ["best buy"]

#industrygroup
healthcare = {"medical":[10,medical],"rehab":[20,rehab]}
hospatility = {"Hotels":[10,hotels],"cpnvention center":[20,conventationcenter]}
tech = {"software":[10,software],"information tech":[20,informationtech]}
marketing ={"marketing":[1,marketing1]}
ed= {"highered":[10,highered],"k-12":[20,k_through_12]}
government ={"louisiana goverment":[10,louisiana]}
political = {"nonprofit":[10,nonprofit]}
goods={"system integrator":[10,si],"equiptment":[20,equiptment]}
utilies = {"entergy":[10,energy],"internet":[20,internet]}
finance = {"bank":[10,bank],"tax":[20,tax],"realty":[30,realty],"insurance":[40,insurance]}
consumergoods = {"electronics":[10,electronics]}



industrygroup = {"healthcare":[1,healthcare],"hospatility":[2,hospatility],"marketing":[3,marketing],
"education":[4,ed],"tech":[5,tech],"government":[6,government],"goods":[7,goods],"utilies":[8,utilies],"finance":[9,finance],"goods":[10,goods], "political":[11,political],"consumergoods":[12,consumergoods] }

industrygroupid = {"healthcare":1,"hospatility":2,"marketing":3,"software":4,
"education":5,"tech":6,"government":7,"goods":8,"utilies":9,"finance":10,"goods":11, "political":12,"consumergoods":13 }
           
def get_gics_of_company(company):
    for industry in industrygroup:
        industrygroupid1 = industrygroup[industry][0]        
        for subindustry in industrygroup[industry][1]:
            industryid1 = str(industrygroupid1)
            subindustryid = str(industrygroup[industry][1][subindustry][0])
            subindustryid1 = industryid1+subindustryid
            for comp in industrygroup[industry][1][subindustry][1]:
                if re.search(comp.lower(),company.lower()):
                    subindustryid1 = industryid1+subindustryid
                    return [industryid1,subindustryid1]

def get_insert(table, data):
	#Returns a proper SQL INSERT statement using the table and values passed
    for i in range(len(data)):
	    if str(data[i]).find("to_date") > -1:
		    pass			
	    elif data[i] is None:
		    data[i] = "NULL"
	    elif type(data[i]) is str:
		    data[i] = "'%s'" % data[i]
	    elif type(data[i]) is int:
		    data[i] = str(data[i])
	    else:
		    data[i] = str(data[i])	
    sql = "INSERT INTO %s VALUES (%s);\n" % (table, ", ".join(data))
    return sql
def get_gics():
    for industry in industrygroup:
        industrygroupid1 = industrygroup[industry][0]
        f2.write(get_insert("gics",[industrygroup[industry][0],industry,None]))        
        for subindustry in industrygroup[industry][1]:
            industryid1 = str(industrygroupid1)
            subindustryid = str(industrygroup[industry][1][subindustry][0])
            subindustryid1 = industryid1+subindustryid
            f2.write(get_insert("gics",[int(subindustryid1),subindustry,industryid1]))
get_gics()

listings = f4.read()
listings = listings.split("]\n")

companies=[]
locations=[]
urls=[]
positions=[]

def get_random_date():
    randmonth = random.randint(1,int(month))
    randyear = random.randint(0,int(year))
    if randyear < 15:
        return None
    else:
        randyear = str(randyear)
    return str(randmonth)+"/20"+randyear

def putcompany_companies(listing):
    company = listing[(listing.index("company=")+8):(listing.index("location")-2)]
    if company not in companies:
        companies.append(company)
        locations.append(listing[(listing.index("location=")+9):(listing.index("dateStr=")-2)])
        urls.append(listing[(listing.index("href=")+5):(listing.index("company=")-2)])
    
def get_company(company):
        gicsid = get_gics_of_company(company)
        location = locations[companies.index(company)]
        url = urls[companies.index(company)]
        cid = companies.index(company)+1
        company = re.sub("&"," and ",company)
        company =re.sub("\'","\'\'",company)
        return [cid, company, location,gicsid[0],gicsid[1], url]

def get_job(listing,pid):
    jid =0
    askill = None
    jobname = listing[(listing.index("title=")+6):(listing.index("id=")-2)]
    description = listing[(listing.index("summary=")+8):]
    company = listing[(listing.index("company=")+8):(listing.index("location")-2)]
    employeetype = "full time"
    for skill in jskills:
        if re.search(skill,jobname.lower()):
            askill = skillz.index(jskills[skill])+1
    jid =jid +1
    if re.search("part time",description.lower()):
        employeetype = "part time"
    cid = companies.index(company)+1
    return [pid+1,pid+1,askill,cid,employeetype]

def get_position(listing,pid):
    askill = None
    paytype = "salary"
    #position
    jobname = listing[(listing.index("title=")+6):(listing.index("id=")-2)]
    for skill in jskills:
        if re.search(skill,jobname.lower()):
            askill = skillz.index(jskills[skill])+1
        else:
            askill = None
    company = listing[(listing.index("company=")+8):(listing.index("location")-2)]
    location = listing[(listing.index("location=")+9):(listing.index("dateStr=")-2)]
    start = listing[(listing.index("dateStr=")+8):(listing.index("ago")-2)]
    end = get_random_date()
    url = listing[(listing.index("href=")+5):(listing.index("company=")-2)]
    description = listing[(listing.index("summary=")+8):]
    description =re.sub("\'","\'\'",description)
    jobname = re.sub("\'","\'\'",jobname)
    paylow = 20000
    payhigh = 30000
    if re.search("hour",description.lower()):
        paytype = "hourly"
        for job in hpaylrange:
            if re.search(job,jobname.lower()):
                paylow = hpaylrange[job]
                payhigh = hpayhrange[job]
    else:
       for job in spaylrange:
            if re.search(job,jobname.lower()):
                paylow = spaylrange[job]
                payhigh = spayhrange[job] 
    return [pid+1,jobname,start,end,paylow,payhigh,paytype,description]

def get_skills_for_job(listing,pid):
    jobname = listing[(listing.index("title=")+6):(listing.index("id=")-2)]
    description = listing[(listing.index("summary=")+8):]
    return [pid+1,random.randint(1,21)]

     
for listing in listings:
    putcompany_companies(listing)
    f1.write(get_insert("position",get_position(listing,listings.index(listing))))
    f2.write(get_insert("position",get_position(listing,listings.index(listing))))
    f2.write(get_insert("requires",get_skills_for_job(listing,listings.index(listing))))
    f2.write(get_insert("requires",get_skills_for_job(listing,listings.index(listing))))
    
for company in companies:
    f2.write(get_insert("company",get_company(company)))
    f6.write(get_insert("company",get_company(company)))

for listing in listings:
    f2.write(get_insert("jobs",get_job(listing,listings.index(listing))))