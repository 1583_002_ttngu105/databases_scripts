import re
import random
people = 50
skills = []
jobs = []
learned = open("skills.sql","r", encoding="utf-8")
positions = open("position.sql","r", encoding="utf-8")
people = 50
learned = learned.read()
positions = positions.read()
variable = re.split("INSERT", learned)
num = re.split("INSERT", positions)

def get_insert(table, data):
	#Returns a proper SQL INSERT statement using the table and values passed
	for i in range(len(data)):
		if str(data[i]).find("to_date") > -1:
			pass			
		elif data[i] is None:
			data[i] = "NULL"
		elif type(data[i]) is str:
			data[i] = "'%s'" % data[i]
		elif type(data[i]) is int:
			data[i] = str(data[i])
		else:
			data[i] = str(data[i])	
	sql = "INSERT INTO %s VALUES (%s);\n" % (table, ", ".join(data))
	return sql

def get_random_phone_number():
	#Returns a random phone number
	return "%s%s%s-%s%s%s-%s%s%s%s" % tuple([random.randint(0,9) for x in range(10)])

def get_random_address():
	addresses = ["Almonaster Avenue","Audubon Place","Baronne Street","Basin Street","Bayou Road","Bienville Street","Bourbon Street","Broad Street","Burgundy Street","Calliope Street","Camp Street","Canal Boulevard","Canal Street","Carondelet Street","Carrollton Avenue","Chartres Street","City Park Avenue","Claiborne Avenue","Conti Street","Dante Street","Dauphine Street","Decatur Street","Desire Street","Dryades Street","Dumaine Street","Earhart Expressway, an extension of Earhart Boulevard","Elysian Fields Avenue","Esplanade Avenue","Exchange Place (pedestrian only)","Felicity Street","Freret Street","Frenchmen Street","Gayoso Street","Gentilly Boulevard","Girod Street","Gravier Street","Henry Clay Avenue","Howard Avenue","Iberville Street","Jackson Avenue","Jeff Davis Parkway","Julia Street","Lafayette Street","Lakeshore Drive","Lee Circle","Louisiana Avenue","Lowerline Street","Loyola Avenue","Magazine Street","Magnolia Street","Marengo Street","Martin Luther King Jr. Boulevard","McAlister Place (pedestrian only)","Napoleon Avenue","Oak Street","Oretha Castle Haley Boulevard","Orleans Street","Pearl Street","Peters Street","Pontchartrain Expressway","Poydras Street","Prytania Street","Rampart Street","Robert E. Lee Boulevard","Royal Street","Simon Bolivar Avenue","St. Ann Street","St. Charles Avenue","St. Claude Avenue","St. Louis Street","St. Peter Street","St. Philip Street","Tchoupitoulas Street","Toulouse Street","Tulane Avenue","Upperline Street","Ursulines Street","Washington Avenue"]
	#zipcodes = [70112, 70113, 70114, 70115, 70116, 70117, 70118, 70119, 70122, 70124, 70125, 70126, 70127, 70128, 70129, 70130, 70131, 70139, 70163]
	stnumber = random.randint(0,20000)
	stname = random.choice(addresses)
	#zip = random.choice(zipcodes)
	return str(stnumber)+" "+stname

def get_random_skill(pid):
	skill = random.randint(1,len(variable)-1)
	return [pid,skill]

def get_past_work(pid):
	job = random.randint(1,len(num)-1)
	return [pid,job]

def get_gender():
	gender = ["M","F"]
	return random.choice(gender)

def get_random_name():
	#Returns a random name
	names = ["Viet", "Veronica", "Truc", "Tre", "Tommy", "Thanh", "Tarik", "Stephen", "Shayna", "Sean", "Sauryha", "Sarah", "Roy", "Ricardo", "Phi", "Patrick", "Pablo", "Nicholas", "Mikolas", "Matthew", "Mark", "Luis", "Lauren", "Kevin", "Joshua", "Joseph", "Jondrielle", "Jonathan", "Jober''t"]
	return random.choice(names)

def get_random_email():
	emaildomains= ["@google.com", "@yahoo.com", "@outlook.com"]
	return str(random.randint(1,40))+random.choice(emaildomains)
have = []
donthave = []
	
def generate_person():
	data = []
	for i in range(people):
		name = get_random_name()
		personid = i+1
		record = [personid,name, get_random_address(),(name+get_random_email()),get_gender(), get_random_phone_number()]
		numskills = random.randint(1,4)
		for k in range (numskills):
			skills.append(get_random_skill(personid))
		#if the person has worked
		number = random.randint(0,2)
		if number == 1 or number == 2:
			rand = random.randint(1,4)
			for j in range(rand):
				jobs.append(get_past_work(personid))
		else:
			placeholder = []
			placeholder.append(personid)
			placeholder.append(None) 
			jobs.append(placeholder)
		data.append(record)
	return data
		
data = {'person': generate_person()}
f = open("person,skills,works.sql", "w")
f.write("DROP TABLE works;\nDROP TABLE hasskill;\nDROP TABLE PERSON;\n")
f.write("\nCREATE TABLE PERSON\n(\n    Personid number(10),\n    Name VARCHAR(50),\n    address varchar(100),\n    email varchar(50) check (email like \'%@%\'),\n    gender varchar(1)check (gender in(\'M\',\'F\')),\n    phonenum varchar(12) check (phonenum like \'%%%-%%%-%%%%\'),\n	unique (personid)\n);\n\nCREATE TABLE works\n(\n	perid number(3),\n	werkid number(3),\n	constraint personfk1 FOREIGN KEY (perid)\n 	References person(personid));\nCREATE TABLE hasskill\n(\n	persid number(3),\n	sid number(3),\n	constraint personfk FOREIGN KEY (persid)References person(personid));\n\n")
#f.write("\nCREATE TABLE works(perid number(3),werkid number(3),constraint personfk1 FOREIGN KEY (personid) References person(perid));\nCREATE TABLE hasskill(perid number(3),sid number(3),constraint personfk FOREIGN KEY (personid) References person(personid));\n")
for key, value in data.items():
	for record in value:
		f.write(get_insert(key, record))
for posit in jobs:
	key = "works"
	f.write(get_insert(key, posit))	
for skill in skills:
	key = "hasskill"
	f.write(get_insert(key, skill))






