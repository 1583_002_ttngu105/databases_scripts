
CREATE TABLE company(
cid number(3) NOT NULL PRIMARY KEY, 
cname varchar(100), 
location varchar(100),
igid number(10),
gid number(10),
Website varchar (1000));
INSERT INTO company VALUES (1, 'Bay State', 'New Orleans, LA', '4', '410', 'https://www.indeed.com/rc/clk?jk=e0f433568fabe8e1&fccid=19aa0bbdd75afe67');
INSERT INTO company VALUES (2, 'AT and T', 'New Orleans, LA', '8', '820', 'https://www.indeed.com/rc/clk?jk=af641a3c3dea6dc6&fccid=25b5166547bbf543');
INSERT INTO company VALUES (3, 'ACI Federal', 'New Orleans, LA', '9', '910', 'https://www.indeed.com/company/ACI-Federal/jobs/Information-Technology-Specialist-c5c93b734fc9673c?fccid=58476d2e8b9c7544');
INSERT INTO company VALUES (4, 'Hyatt', 'New Orleans, LA', '2', '210', 'https://www.indeed.com/rc/clk?jk=cb8b9a04e68084af&fccid=b385273868d506a9');
INSERT INTO company VALUES (5, 'The Troubadour', 'New Orleans, LA 70112 (Central Business District area)', '2', '210', 'https://www.indeed.com/rc/clk?jk=5a86c75262b3bdaf&fccid=0de594717975faf2');
INSERT INTO company VALUES (6, 'Sparkhound', 'Hammond, LA', '5', '510', 'https://www.indeed.com/rc/clk?jk=08b558b010eebe13&fccid=81965b70260b0455');
INSERT INTO company VALUES (7, 'Children''s Hospital, New Orleans', 'New Orleans, LA', '1', '110', 'https://www.indeed.com/rc/clk?jk=919461f5732612ea&fccid=ff8ce7baeed40030');
INSERT INTO company VALUES (8, 'Odyssey House Louisiana, Inc.', 'New Orleans, LA 70119 (Bayou Saint John area)', '1', '120', 'https://www.indeed.com/company/Odyssey-House-Louisiana,-Inc./jobs/PC-Technician-4a1f104801ea2e20?fccid=a9b920cf23c58d84');
INSERT INTO company VALUES (9, 'JK2S Solutions', 'New Orleans, LA', '5', '510', 'https://www.indeed.com/company/JK2S/jobs/IT-Specialist-a98d514e0a89d602?fccid=2b0e2ec249ca6690');
INSERT INTO company VALUES (10, 'Destination Hotels  and  Resorts', 'New Orleans, LA', '2', '210', 'https://www.indeed.com/rc/clk?jk=f9c4d675beee03de&fccid=e2dea33ac04eacd7');
INSERT INTO company VALUES (11, 'Fresenius Medical Care', 'Metairie, LA', '1', '110', 'https://www.indeed.com/rc/clk?jk=a67a6895f6024c79&fccid=8de1633f2f8eae1d');
INSERT INTO company VALUES (12, 'CGI', 'Hammond, LA', '5', '520', 'https://www.indeed.com/rc/clk?jk=d6a2b63918c2f67a&fccid=d2841a5c0380b93d');
INSERT INTO company VALUES (13, 'Epsilon Inc.', 'New Orleans, LA 70146 (Lake Terrace And Oaks area)', '5', '520', 'https://www.indeed.com/rc/clk?jk=a414d2e6fdba1a4a&fccid=765e0365309ddf72');
INSERT INTO company VALUES (14, 'Network Runners, Inc.', 'New Orleans, LA', '5', '520', 'https://www.indeed.com/rc/clk?jk=2932fd8cd8f3e5d0&fccid=957fdc33884615ec');
INSERT INTO company VALUES (15, 'First NBC Bank', 'New Orleans, LA', '9', '910', 'https://www.indeed.com/rc/clk?jk=0667097456bbbb89&fccid=4cd44f8ed180811d');
INSERT INTO company VALUES (16, 'CACI', 'Stennis Space Center, MS 39529', '5', '520', 'https://www.indeed.com/rc/clk?jk=facbfcd6eae2116d&fccid=290a4498a64fc044');
INSERT INTO company VALUES (17, 'Best Buy', 'Covington, LA 70433', '12', '1210', 'https://www.indeed.com/rc/clk?jk=a640d8e4b8d90b37&fccid=027d7cca25a5d14f');
INSERT INTO company VALUES (18, 'Terrebonne General Medical Center', 'Houma, LA', '1', '110', 'https://www.indeed.com/rc/clk?jk=c0f455dfbbec71b7&fccid=b3eb08a7b54e330b');
INSERT INTO company VALUES (19, 'Tulane University', 'New Orleans, LA', '4', '410', 'https://www.indeed.com/rc/clk?jk=b5bac8d5b2fe0ad9&fccid=32f1f726276ba707');
INSERT INTO company VALUES (20, 'General Electric', 'New Orleans, LA 70121', '10', '1020', 'https://www.indeed.com/rc/clk?jk=72335b196af723f7&fccid=c5c99ec01e2125aa');
INSERT INTO company VALUES (21, 'Ultimate Technical Solutions, Inc.', 'New Orleans, LA 70123', '5', '510', 'https://www.indeed.com/company/Ultimate-Technical-Solutions,-Inc./jobs/Help-Desk-Technician-7e6c820a31c1167d?fccid=564040c6a12a690f');
INSERT INTO company VALUES (22, 'Dell', 'New Orleans, LA', '10', '1010', 'https://www.indeed.com/rc/clk?jk=8008ed3ac9a4b99c&fccid=0918a251e6902f97');
INSERT INTO company VALUES (23, 'General Dynamics Information Technology', 'Stennis Space Center, MS', '5', '520', 'https://www.indeed.com/rc/clk?jk=84f39a6d77b6d4c7&fccid=11caadcdc98800d4');
INSERT INTO company VALUES (24, 'Kofax', 'Metairie, LA', '5', '510', 'https://www.indeed.com/rc/clk?jk=b5fe46f16f49f138&fccid=733ea912d58257f6');
INSERT INTO company VALUES (25, 'Gilsbar Holdings', 'Covington, LA', '9', '940', 'https://www.indeed.com/rc/clk?jk=32633cdd1bbc0f8d&fccid=591edce286e3ff98');
INSERT INTO company VALUES (26, 'Gilsbar', 'Covington, LA 70433', '9', '940', 'https://www.indeed.com/rc/clk?jk=079874832df2424d&fccid=79f204bea5e3ca6a');
INSERT INTO company VALUES (27, 'MECO (Mechanical Equipment Company Inc)', 'Mandeville, LA 70471', '10', '1020', 'https://www.indeed.com/company/MECO-(Mechanical-Equipment-Company-Inc)/jobs/PC-Technician-5dbe78cd759f9041?fccid=cefe145147cc4f55');
INSERT INTO company VALUES (28, 'NRLC', 'Covington, LA', '11', '1110', 'https://www.indeed.com/company/NRLC/jobs/IT-Consultant-c8e2c5238e583682?fccid=b1a5eca9eec687bd');
INSERT INTO company VALUES (29, 'comtecinfo', 'New Orleans, LA', '5', '520', 'https://www.indeed.com/rc/clk?jk=12667e1eec8f6880&fccid=e36b62be8424611e');
INSERT INTO company VALUES (30, 'XG Security', 'Metairie, LA', '5', '520', 'https://www.indeed.com/company/XG-Security/jobs/Network-Technician-66d4809724f46180?fccid=e11bfb886fc2ed49');
INSERT INTO company VALUES (31, 'Microsoft', 'New Orleans, LA', '5', '510', 'https://www.indeed.com/rc/clk?jk=1dfac62a08dcbf98&fccid=734cb5a01ee60f80');
INSERT INTO company VALUES (32, 'SpikeIT Global Solutions', 'New Orleans, LA', '5', '510', 'https://www.indeed.com/company/SpikeIT-Global-Solutions/jobs/Cyber-Security-Analyst-0ba17389d1872fd7?fccid=f20f77a920649633');
INSERT INTO company VALUES (33, 'Volunteers of America Greater New Orleans', 'New Orleans, LA', '1', '120', 'https://www.indeed.com/rc/clk?jk=17d7017196a0b413&fccid=9cabce648c17e480');
INSERT INTO company VALUES (34, 'Louisiana State Government', 'New Orleans, LA', '6', '610', 'https://www.indeed.com/rc/clk?jk=37da381b0797ba14&fccid=e0f35a124e6f113c');
INSERT INTO company VALUES (35, 'Knight Point Systems', 'Stennis Space Center, MS', '5', '520', 'https://www.indeed.com/rc/clk?jk=fdbede9aee82d101&fccid=c0309bed6539b87e');
INSERT INTO company VALUES (36, 'Volunteers of America - New Orleans', 'New Orleans, LA', '1', '120', 'https://www.indeed.com/rc/clk?jk=cac7b17a7a8184a2&fccid=2945920d6a3cdd92');
INSERT INTO company VALUES (37, 'Entergy', 'Killona, LA', '8', '810', 'https://www.indeed.com/rc/clk?jk=c68c2a08ba449c0f&fccid=4ba02d8aeaa3e74a');
INSERT INTO company VALUES (38, 'CapinCrouse LLP', 'New Orleans, LA', '9', '920', 'https://www.indeed.com/rc/clk?jk=ad086c2eba85d17a&fccid=8eba5bacc3d96bf6');
INSERT INTO company VALUES (39, 'Capriccio Software, Inc.', 'New Orleans, LA', '5', '510', 'https://www.indeed.com/rc/clk?jk=bcaa471f23723e5d&fccid=504120812bd53c30');
INSERT INTO company VALUES (40, 'Hewlett Packard Enterprise', 'New Orleans, LA', '10', '1010', 'https://www.indeed.com/rc/clk?jk=8c911242b0c0bce9&fccid=216eb700022de6f6');
INSERT INTO company VALUES (41, 'Ochsner Health System', 'New Orleans, LA', '1', '110', 'https://www.indeed.com/rc/clk?jk=9f933808df1a76ff&fccid=ef34ff29d760a6f2');
INSERT INTO company VALUES (42, 'Deloitte', 'New Orleans, LA', '9', '920', 'https://www.indeed.com/rc/clk?jk=92e081dd3d0651e2&fccid=9e215d88a6b33622');
INSERT INTO company VALUES (43, 'K2 Realty', 'Covington, LA 70433', '9', '930', 'https://www.indeed.com/company/K2-realty/jobs/Technical-Support-55a188f4e547ba41?fccid=a8698a8ab9b0280c');
INSERT INTO company VALUES (44, 'PSI Pax', 'New Orleans, LA', '5', '520', 'https://www.indeed.com/rc/clk?jk=4328815d64741951&fccid=758bc63e5b967bdf');
INSERT INTO company VALUES (45, 'Anntoine Marketing and Design', 'New Orleans, LA', '3', '31', 'https://www.indeed.com/company/Anntoine-Marketing-and-Design/jobs/Front-End-Web-Developer-30454b34f112d0e6?fccid=43d9c59f8bc9a58e');
INSERT INTO company VALUES (46, 'Network Logic, LLC', 'Mandeville, LA 70471', '5', '520', 'https://www.indeed.com/company/Network-Logic,-LLC/jobs/Network-Tech-17a6e7274dca0c46?fccid=eec4652733761613');
INSERT INTO company VALUES (47, 'Bellwether Technology', 'New Orleans, LA 70130 (Lower Garden District area)', '5', '520', 'https://www.indeed.com/rc/clk?jk=0070c89b851f433b&fccid=9246a04bc0770376');
INSERT INTO company VALUES (48, 'MILVETS Systems Technology, Inc.', 'New Orleans, LA', '5', '520', 'https://www.indeed.com/rc/clk?jk=98cc3c4cb60f0c37&fccid=bc61725f7ac26e7b');
INSERT INTO company VALUES (49, 'Catapult Consultants, LLC', 'New Orleans, LA', '5', '510', 'https://www.indeed.com/rc/clk?jk=a8905826118cc538&fccid=2ef1c0aa55f82227');
INSERT INTO company VALUES (50, 'CSRA', 'Stennis Space Center, MS', '5', '520', 'https://www.indeed.com/rc/clk?jk=181c0ff969418c22&fccid=0e18721ba57279e5');
INSERT INTO company VALUES (51, 'Pan-American Life Insurance Group', 'New Orleans, LA', '9', '940', 'https://www.indeed.com/rc/clk?jk=d127edaf56c77b1f&fccid=44a60fd5f29788e0');
INSERT INTO company VALUES (52, 'Aventure Technologies, LLC', 'New Orleans, LA', '5', '520', 'https://www.indeed.com/rc/clk?jk=18270403849b0428&fccid=265aa8abb6daba68');
INSERT INTO company VALUES (53, 'In-Telecom Consulting', 'Slidell, LA', '5', '510', 'https://www.indeed.com/company/In--Telecom-Consulting/jobs/Help-Desk-Technician-6a74f1833174041c?fccid=eb755b9dde89fd1a');
INSERT INTO company VALUES (54, 'Abacus Technology Corporation', 'Stennis Space Center, MS', '5', '520', 'https://www.indeed.com/rc/clk?jk=8e7a6d1c5b0523dd&fccid=9b23f360c9f83ff0');
INSERT INTO company VALUES (55, 'Creole Gardens Guesthouse', 'New Orleans, LA', '2', '210', 'https://www.indeed.com/rc/clk?jk=c8b78d1d3699e6db&fccid=259c1488e8517c41');
INSERT INTO company VALUES (56, 'Whetstone Education', 'New Orleans, LA', '4', '420', 'https://www.indeed.com/company/Whetstone-Education/jobs/Software-Developer-74a1f84251acf9e9?fccid=7506d823742f237a');
INSERT INTO company VALUES (57, 'H2 Performance Consulting', 'New Orleans, LA', '5', '510', 'https://www.indeed.com/rc/clk?jk=3c7ca62ea5c5d6c9&fccid=e26388c1e2a22dd7');
INSERT INTO company VALUES (58, 'Laitram LLC', 'New Orleans, LA 70123', '10', '1020', 'https://www.indeed.com/rc/clk?jk=405a715ee81f5b02&fccid=06c0065578ad0db6');
INSERT INTO company VALUES (59, 'Booz Allen Hamilton', 'New Orleans, LA', '5', '520', 'https://www.indeed.com/rc/clk?jk=49ce91b73e6da01a&fccid=4e041af1d0af1bc8');
INSERT INTO company VALUES (60, 'La Health Solutions', 'Slidell, LA 70458', '1', '110', 'https://www.indeed.com/company/La-Health-Solutions/jobs/Medical-Data-Entry-Genius-0425332037aec768?fccid=55b611e25019dd77');
INSERT INTO company VALUES (61, 'Hornbeck Offshore', 'Covington, LA 70433', '10', '1020', 'https://www.indeed.com/company/Hornbeck-Offshore/jobs/IT-Help-Desk-b457e80657cfecc2?fccid=270ec55cf0d510e1');
INSERT INTO company VALUES (62, 'zlien', 'New Orleans, LA 70130 (Lower Garden District area)', '5', '510', 'https://www.indeed.com/rc/clk?jk=eea7d6bb82bdda80&fccid=bac8e1cdec7d5d6c');
INSERT INTO company VALUES (63, 'Grand Family Dentistry', 'Mandeville, LA', '1', '110', 'https://www.indeed.com/company/Grand-Family-Dentistry/jobs/Office-Administrator-4dac0b089960324f?fccid=5e5fc9c2845d5f54');
INSERT INTO company VALUES (64, 'Ernest N. Morial Convention Center', 'New Orleans, LA', '2', '220', 'https://www.indeed.com/rc/clk?jk=7a0074fbf25a929b&fccid=ada99f53cdd1f9cd');
INSERT INTO company VALUES (65, 'The Lynd Company', 'New Orleans, LA', '9', '930', 'https://www.indeed.com/rc/clk?jk=c215dae41f5c3204&fccid=a6273440d811796a');
INSERT INTO company VALUES (66, 'PosiGen, LLC', 'Jefferson, LA 70121', '10', '1020', 'https://www.indeed.com/rc/clk?jk=d4fdb9499f53ddc4&fccid=a19e8da745403832');
