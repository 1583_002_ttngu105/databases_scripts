-------------------------------------------------13---------------------------------------------------------------
select personid, name, pname from (position inner join works on pid = werkid)inner join person on perid = personid
where personid = 23;


-----------------------------------------------14------------------------------------------------------------------
select person.personid, name, pname, sdates, edates from (works inner join person on person.personid = works.perid)inner join position on works.werkid = position.pid
where edates is not NULL
order by personid;

------------------------------------------------------15----------------------------------------------------------
select DISTINCT personid, name from person join works on personid = perid
    where personid in (select personid from person inner join works on personid = perid
where works.werkid is null) and works.werkid is not NULL ;

----------------------------------16-------------------------------------------------------------------------------
select gid, round(avg(paylow+payhigh/2),2) 
from (jobs inner join company on cid 
= jobs.companyid) 
inner join position on pid = posid
where jobs.employeetype = 'full time'
group by gid
order by gid;

-------------------------------------17-----------------------------------------------------------------------

--q1 employer
select cname from company 
where cid in
(select companyid from((works join person on personid = perid)join jobs on jid = werkid) inner join position on pid = jobs.posid
group by companyid
having count(posid) = 
((select max(jerbs) 
from(select companyid, count(posid) as jerbs from (select * from works inner join jobs on werkid = jobs.jid 
where werkid is not null)
GROUP by companyid
))));

--q2 industry 
select gname from gics
where gicsid =
(select gid from company 
where cid in
(select companyid from((works join person on personid = perid)join jobs on jid = werkid) inner join position on pid = jobs.posid
group by companyid
having count(posid) = 
((select max(jerbs) 
from(select companyid, count(posid) as jerbs from (select * from works inner join jobs on werkid = jobs.jid 
where werkid is not null)
GROUP by companyid
)))));

--q3 industry group
select gname from gics
where gicsid =
(select igid from company 
where cid in
(select companyid from((works join person on personid = perid)join jobs on jid = werkid) inner join position on pid = jobs.posid
group by companyid
having count(posid) = 
((select max(jerbs) 
from(select companyid, count(posid) as jerbs from (select * from works inner join jobs on werkid = jobs.jid 
where werkid is not null)
GROUP by companyid
)))));

-------------------------------20--------------------------------------------------------------------------
select pname from position 
where payhigh in (select max(payhigh) from 
    (hasskill inner join jobs on hasskill.sid = jobs.skillid) 
        join position on jobs.posid = position.pid
            where position.pid = posid and persid = 4);

-------------------------------------------21---------------------------------------------------------------
select name, person.email from ((hasskill inner join jobs on hasskill.sid = jobs.skillid) 
join position on jobs.posid = position.pid)inner join person on hasskill.persid = person.personid
where position.pid = 3;

--------------------------------------------22----------------------------------------------------------------

drop table k;
create table k(
    Persoid number(10),
    posid number(3)
);
insert into k
select personid,pid from(select personid,pid,abs(count(sid)-2)as skillz from (person join hasskill on hasskill.persid = person.personid)join
(select pid,skillcode from requires join position on requires.rsid = position.pid)on sid = skillcode
group by personid,pid
order by personid)
where skillz !=0 and pid = 1
;

select * from k
order by persoid
;